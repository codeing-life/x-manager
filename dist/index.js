import { Desktop } from "./desktop";
import { DEFAULT_THEME, MAP_TO_CSS } from "./theme";
const THEMES = new Map();
THEMES.set("default", DEFAULT_THEME);
export { Desktop, THEMES, MAP_TO_CSS };
//# sourceMappingURL=index.js.map