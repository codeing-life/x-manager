import {Desktop, THEMES, MAP_TO_CSS} from "./../dist/index.js";

const THEME = THEMES.get("default");

window.desktop = new Desktop(document.getElementById('desktop'), MAP_TO_CSS(THEME.get("desktop")));
