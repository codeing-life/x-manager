export let DEFAULT_THEME: Map<String, Map<String, String>> = new Map();

export function MAP_TO_CSS(MAP: Map<String, String>): string{
  let output = "";
  for(let [property, value] of MAP){
    output += `${property}: ${value};`;
  }
  return output;
}

//Desktop Theme
{
  let desktop: Map<String, String> = new Map();
  desktop.set("background", "radial-gradient(circle at 75% 20%, rgb(251, 255, 189) 0%, rgb(243, 255, 0) 15%, rgba(0,212,255,0) 20%), linear-gradient(lightblue 0% 60%, blue 65% 100%)");
  DEFAULT_THEME.set("desktop", desktop);
}
